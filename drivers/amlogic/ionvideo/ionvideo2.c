/*
 * Ion Video driver - This code emulates a real video device with v4l2 api,
 * used for surface video display.
 *
 *Author: Shuai Cao <shuai.cao@amlogic.com>
 *
 */
#include "ionvideo2.h"

#define IONVIDEO_MODULE_NAME "ionvideo2"

#define IONVIDEO_VERSION "1.0"
#define RECEIVER_NAME "ionvideo2"

static int is_actived = 0;

static unsigned video_nr = 14;

static unsigned debug = 0;
module_param(debug, uint, 0644);
MODULE_PARM_DESC(debug, "activates debug info");

static unsigned int vid_limit = 16;
module_param(vid_limit, uint, 0644);
MODULE_PARM_DESC(vid_limit, "capture memory limit in megabytes");

static unsigned int skip_frames = 0;
module_param(skip_frames, uint, 0664);
MODULE_PARM_DESC(skip_frames, "skip frames");

static unsigned int deinterlaced = 0;
module_param(deinterlaced, uint, 0664);
MODULE_PARM_DESC(deinterlaced, "deinterlaced");

static unsigned get_ionvideo2_debug(void);

extern int v4l_to_ge2d_format(int v4l2_format);

struct amlvideo3_output {
    int canvas_id;
    int width;
    int height;
    u32 v4l2_format;
    int angle;
    int* frame;
};

static void output_axis_adjust(int src_w, int src_h, int* dst_w, int* dst_h, int angle) {
    int w = 0, h = 0,disp_w = 0, disp_h =0;
    disp_w = *dst_w;
    disp_h = *dst_h;
    if (angle %180 !=0) {
        h = min((int)src_w, disp_h);
        w = src_h * h / src_w;
        if(w > disp_w ){
            h = (h * disp_w)/w ;
            w = disp_w;
        }
    }else{
        if ((src_w < disp_w) && (src_h < disp_h)) {
            w = src_w;
            h = src_h;
        } else if ((src_w * disp_h) > (disp_w * src_h)) {
            w = disp_w;
            h = disp_w * src_h / src_w;
        } else {
            h = disp_h;
            w = disp_h * src_w / src_h;
        }
    }
    *dst_w = w;
    *dst_h = h;
}

static int get_input_format_no_interlace(vframe_t* vf)
{
    int format= GE2D_FORMAT_M24_NV21;
    if(vf->type&VIDTYPE_VIU_422){
        format =  GE2D_FORMAT_S16_YUV422;
    }else if(vf->type&VIDTYPE_VIU_NV21){
        format =  GE2D_FORMAT_M24_NV21;
    } else{
        format =  GE2D_FORMAT_M24_YUV420;
    }
    return format;
}

static int get_interlace_input_format(vframe_t* vf, struct amlvideo3_output* output)
{
    int format= GE2D_FORMAT_M24_NV21;
    if(vf->type&VIDTYPE_VIU_422){
        format =  GE2D_FORMAT_S16_YUV422;
        if(vf->height >= output->height<<2){
            format =  GE2D_FORMAT_S16_YUV422|(GE2D_FORMAT_S16_YUV422T & (3<<3));
        }
    }else if(vf->type&VIDTYPE_VIU_NV21){
        format =  GE2D_FORMAT_M24_NV21;
        if(vf->height >= output->height<<2){
            format =  GE2D_FORMAT_M24_NV21|(GE2D_FORMAT_M24_NV21T & (3<<3));
        }
    } else{
        format =  GE2D_FORMAT_M24_YUV420;
        if(vf->height >= output->height<<2){
            format =  GE2D_FORMAT_M24_YUV420|(GE2D_FORMAT_M24_YUV420T & (3<<3));
        }
    }
    return format;
}

static int get_output_format(int v4l2_format)
{
    int format = GE2D_FORMAT_S24_YUV444;
    switch(v4l2_format){
        case V4L2_PIX_FMT_RGB565X:
            format = GE2D_FORMAT_S16_RGB_565;
            break;
        case V4L2_PIX_FMT_YUV444:
            format = GE2D_FORMAT_S24_YUV444;
            break;
        case V4L2_PIX_FMT_VYUY:
            format = GE2D_FORMAT_S16_YUV422;
            break;
        case V4L2_PIX_FMT_BGR24:
            format = GE2D_FORMAT_S24_RGB ;
            break;
        case V4L2_PIX_FMT_RGB24:
            format = GE2D_FORMAT_S24_BGR;
            break;
        case V4L2_PIX_FMT_NV12:
            format = GE2D_FORMAT_M24_NV12;
            break;
        case V4L2_PIX_FMT_NV21:
            format = GE2D_FORMAT_M24_NV21;
            break;
        case V4L2_PIX_FMT_YUV420:
        case V4L2_PIX_FMT_YVU420:
            format = GE2D_FORMAT_S8_Y;
            break;
        default:
            break;
    }
    return format;
}

static int get_input_format(vframe_t* vf)
{
    int format= GE2D_FORMAT_M24_NV21;
    if(vf->type&VIDTYPE_VIU_422){
        if(vf->type &VIDTYPE_INTERLACE_BOTTOM){
            format =  GE2D_FORMAT_S16_YUV422|(GE2D_FORMAT_S16_YUV422B & (3<<3));
        }else if(vf->type &VIDTYPE_INTERLACE_TOP){
            format =  GE2D_FORMAT_S16_YUV422|(GE2D_FORMAT_S16_YUV422T & (3<<3));
        }else{
            format =  GE2D_FORMAT_S16_YUV422;
        }
    }else if(vf->type&VIDTYPE_VIU_NV21){
        if(vf->type &VIDTYPE_INTERLACE_BOTTOM){
            format =  GE2D_FORMAT_M24_NV21|(GE2D_FORMAT_M24_NV21B & (3<<3));
        }else if(vf->type &VIDTYPE_INTERLACE_TOP){
            format =  GE2D_FORMAT_M24_NV21|(GE2D_FORMAT_M24_NV21T & (3<<3));
        }else{
            format =  GE2D_FORMAT_M24_NV21;
        }
    } else{
        if(vf->type &VIDTYPE_INTERLACE_BOTTOM){
            format =  GE2D_FORMAT_M24_YUV420|(GE2D_FMT_M24_YUV420B & (3<<3));
        }else if(vf->type &VIDTYPE_INTERLACE_TOP){
            format =  GE2D_FORMAT_M24_YUV420|(GE2D_FORMAT_M24_YUV420T & (3<<3));
        }else{
            format =  GE2D_FORMAT_M24_YUV420;
        }
    }
    return format;
}

int amlvideo3_ge2d_interlace_two_canvasAddr_process(vframe_t* vf, ge2d_context_t *context,config_para_ex_t* ge2d_config, struct amlvideo3_output* output)
{
    int src_top ,src_left ,src_width, src_height;
    int dst_top ,dst_left ,dst_width, dst_height;
    canvas_t cs0,cs1,cs2,cd;
    int current_mirror = 0;
    int cur_angle = 0;
    int output_canvas  = vf->canvas0Addr;

        //============================
        //      top field
        //============================
    src_top = 0;
    src_left = 0;
    src_width = vf->width;
    //src_height = vf->height/2;
    src_height = vf->height;

    dst_top = 0;
    dst_left = 0;
    dst_width = output->width;
    dst_height = output->height;

    current_mirror = 0;
    cur_angle = output->angle;
    if(current_mirror == 1)
        cur_angle = (360 - cur_angle%360);
    else
        cur_angle = cur_angle%360;

    if(src_width<src_height)
        cur_angle = (cur_angle+90)%360;

       output_axis_adjust(src_width,src_height,&dst_width,&dst_height,cur_angle);
    dst_top = (output->height-dst_height)/2;
    dst_left = (output->width-dst_width)/2;
    dst_top = dst_top&0xfffffffe;
    dst_left = dst_left&0xfffffffe;
    /* data operating. */

    memset(ge2d_config,0,sizeof(config_para_ex_t));
    if((dst_left!=output->frame[0])||(dst_top!=output->frame[1])
      ||(dst_width!=output->frame[2])||(dst_height!=output->frame[3])){
        ge2d_config->alu_const_color= 0;//0x000000ff;
        ge2d_config->bitmask_en  = 0;
        ge2d_config->src1_gb_alpha = 0;//0xff;
        ge2d_config->dst_xy_swap = 0;

        canvas_read(output_canvas&0xff,&cd);
        ge2d_config->src_planes[0].addr = cd.addr;
        ge2d_config->src_planes[0].w = cd.width;
        ge2d_config->src_planes[0].h = cd.height;
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;

        ge2d_config->src_key.key_enable = 0;
        ge2d_config->src_key.key_mask = 0;
        ge2d_config->src_key.key_mode = 0;

        ge2d_config->src_para.canvas_index=output_canvas;
        ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->src_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->src_para.fill_color_en = 0;
        ge2d_config->src_para.fill_mode = 0;
        ge2d_config->src_para.x_rev = 0;
        ge2d_config->src_para.y_rev = 0;
        ge2d_config->src_para.color = 0;
        ge2d_config->src_para.top = 0;
        ge2d_config->src_para.left = 0;
        ge2d_config->src_para.width = output->width;
        ge2d_config->src_para.height = output->height;

        ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;

        ge2d_config->dst_para.canvas_index=output_canvas;
        ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->dst_para.fill_color_en = 0;
        ge2d_config->dst_para.fill_mode = 0;
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
        ge2d_config->dst_para.color = 0;
        ge2d_config->dst_para.top = 0;
        ge2d_config->dst_para.left = 0;
        ge2d_config->dst_para.width = output->width;
        ge2d_config->dst_para.height = output->height;

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -2;
        }
        fillrect(context, 0, 0, output->width, output->height, (ge2d_config->dst_para.format&GE2D_FORMAT_YUV)?0x008080ff:0);
        output->frame[0] = dst_left;
        output->frame[1] = dst_top;
        output->frame[2] = dst_width;
        output->frame[3] = dst_height;
        memset(ge2d_config,0,sizeof(config_para_ex_t));
    }
    ge2d_config->alu_const_color= 0;//0x000000ff;
    ge2d_config->bitmask_en  = 0;
    ge2d_config->src1_gb_alpha = 0;//0xff;
    ge2d_config->dst_xy_swap = 0;

    canvas_read(vf->canvas0Addr&0xff,&cs0);
    canvas_read((vf->canvas0Addr>>8)&0xff,&cs1);
    canvas_read((vf->canvas0Addr>>16)&0xff,&cs2);
    ge2d_config->src_planes[0].addr = cs0.addr;
    ge2d_config->src_planes[0].w = cs0.width;
    ge2d_config->src_planes[0].h = cs0.height;
    ge2d_config->src_planes[1].addr = cs1.addr;
    ge2d_config->src_planes[1].w = cs1.width;
    ge2d_config->src_planes[1].h = cs1.height;
    ge2d_config->src_planes[2].addr = cs2.addr;
    ge2d_config->src_planes[2].w = cs2.width;
    ge2d_config->src_planes[2].h = cs2.height;
    canvas_read(output_canvas&0xff,&cd);
    ge2d_config->dst_planes[0].addr = cd.addr;
    ge2d_config->dst_planes[0].w = cd.width;
    ge2d_config->dst_planes[0].h = cd.height;
    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index=vf->canvas0Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_input_format(vf);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    ge2d_config->src_para.height = vf->height/2;
    /* printk("vf_width is %d , vf_height is %d \n",vf->width ,vf->height); */
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.canvas_index = output_canvas&0xff;

    if((output->v4l2_format!= V4L2_PIX_FMT_YUV420)
        && (output->v4l2_format != V4L2_PIX_FMT_YVU420))
        ge2d_config->dst_para.canvas_index = output_canvas;

    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.format = (get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420T & (3<<3));
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.x_rev = 0;
    ge2d_config->dst_para.y_rev = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = output->width;
    ge2d_config->dst_para.height = output->height/2;

    if(current_mirror==1){
        ge2d_config->dst_para.x_rev = 1;
        ge2d_config->dst_para.y_rev = 0;
    }else if(current_mirror==2){
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 1;
    }else{
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
    }

    if(cur_angle==90){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.x_rev ^= 1;
    }else if(cur_angle==180){
        ge2d_config->dst_para.x_rev ^= 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }else if(cur_angle==270){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }

    if(ge2d_context_config_ex(context,ge2d_config)<0) {
        printk("++ge2d configing error.\n");
        return -1;
    }
    stretchblt_noalpha(context,src_left ,src_top/2 ,src_width, src_height/2,output->frame[0],output->frame[1]/2,output->frame[2],output->frame[3]/2);

    /* for cr of  yuv420p or yuv420sp. */
    if((output->v4l2_format==V4L2_PIX_FMT_YUV420)
        ||(output->v4l2_format==V4L2_PIX_FMT_YVU420)){
        /* for cb. */
        canvas_read((output_canvas>>8)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>8)&0xff;
        ge2d_config->dst_para.format=(GE2D_FORMAT_S8_CB|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420T & (3<<3));
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/4;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/4,output->frame[2]/2,output->frame[3]/4);
    }
    /* for cb of yuv420p or yuv420sp. */
    if(output->v4l2_format==V4L2_PIX_FMT_YUV420||
        output->v4l2_format==V4L2_PIX_FMT_YVU420) {
        canvas_read((output_canvas>>16)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>16)&0xff;
        ge2d_config->dst_para.format=(GE2D_FORMAT_S8_CR|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420T & (3<<3));
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/4;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/4,output->frame[2]/2,output->frame[3]/4);
    }

     //============================
    //      bottom field
    //============================
    output_canvas  = vf->canvas1Addr;
    src_top = 0;
    src_left = 0;
    src_width = vf->width;
    src_height = vf->height;

    dst_top = 0;
    dst_left = 0;
    dst_width = output->width;
    dst_height = output->height;

    current_mirror = 0;
    cur_angle = output->angle;
    if(current_mirror == 1)
        cur_angle = (360 - cur_angle%360);
    else
        cur_angle = cur_angle%360;

    if(src_width<src_height)
        cur_angle = (cur_angle+90)%360;

       output_axis_adjust(src_width,src_height,&dst_width,&dst_height,cur_angle);
    dst_top = (output->height-dst_height)/2;
    dst_left = (output->width-dst_width)/2;
    dst_top = dst_top&0xfffffffe;
    dst_left = dst_left&0xfffffffe;
    /* data operating. */

    memset(ge2d_config,0,sizeof(config_para_ex_t));
    if((dst_left!=output->frame[0])||(dst_top!=output->frame[1])
      ||(dst_width!=output->frame[2])||(dst_height!=output->frame[3])){
        ge2d_config->alu_const_color= 0;//0x000000ff;
        ge2d_config->bitmask_en  = 0;
        ge2d_config->src1_gb_alpha = 0;//0xff;
        ge2d_config->dst_xy_swap = 0;

        canvas_read(output_canvas&0xff,&cd);
        ge2d_config->src_planes[0].addr = cd.addr;
        ge2d_config->src_planes[0].w = cd.width;
        ge2d_config->src_planes[0].h = cd.height;
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;

        ge2d_config->src_key.key_enable = 0;
        ge2d_config->src_key.key_mask = 0;
        ge2d_config->src_key.key_mode = 0;

        ge2d_config->src_para.canvas_index=output_canvas;
        ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->src_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->src_para.fill_color_en = 0;
        ge2d_config->src_para.fill_mode = 0;
        ge2d_config->src_para.x_rev = 0;
        ge2d_config->src_para.y_rev = 0;
        ge2d_config->src_para.color = 0;
        ge2d_config->src_para.top = 0;
        ge2d_config->src_para.left = 0;
        ge2d_config->src_para.width = output->width;
        ge2d_config->src_para.height = output->height;

        ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;

        ge2d_config->dst_para.canvas_index=output_canvas;
        ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->dst_para.fill_color_en = 0;
        ge2d_config->dst_para.fill_mode = 0;
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
        ge2d_config->dst_para.color = 0;
        ge2d_config->dst_para.top = 0;
        ge2d_config->dst_para.left = 0;
        ge2d_config->dst_para.width = output->width;
        ge2d_config->dst_para.height = output->height;

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -2;
        }
        fillrect(context, 0, 0, output->width, output->height, (ge2d_config->dst_para.format&GE2D_FORMAT_YUV)?0x008080ff:0);
        output->frame[0] = dst_left;
        output->frame[1] = dst_top;
        output->frame[2] = dst_width;
        output->frame[3] = dst_height;
        memset(ge2d_config,0,sizeof(config_para_ex_t));
    }
    ge2d_config->alu_const_color= 0;//0x000000ff;
    ge2d_config->bitmask_en  = 0;
    ge2d_config->src1_gb_alpha = 0;//0xff;
    ge2d_config->dst_xy_swap = 0;

    canvas_read(vf->canvas1Addr&0xff,&cs0);
    canvas_read((vf->canvas1Addr>>8)&0xff,&cs1);
    canvas_read((vf->canvas1Addr>>16)&0xff,&cs2);
    ge2d_config->src_planes[0].addr = cs0.addr;
    ge2d_config->src_planes[0].w = cs0.width;
    ge2d_config->src_planes[0].h = cs0.height;
    ge2d_config->src_planes[1].addr = cs1.addr;
    ge2d_config->src_planes[1].w = cs1.width;
    ge2d_config->src_planes[1].h = cs1.height;
    ge2d_config->src_planes[2].addr = cs2.addr;
    ge2d_config->src_planes[2].w = cs2.width;
    ge2d_config->src_planes[2].h = cs2.height;
    canvas_read(output_canvas&0xff,&cd);
    ge2d_config->dst_planes[0].addr = cd.addr;
    ge2d_config->dst_planes[0].w = cd.width;
    ge2d_config->dst_planes[0].h = cd.height;
    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index=vf->canvas1Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_input_format(vf);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    ge2d_config->src_para.height = vf->height/2;
    /* printk("vf_width is %d , vf_height is %d \n",vf->width ,vf->height); */
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.canvas_index = output_canvas&0xff;

    if((output->v4l2_format!= V4L2_PIX_FMT_YUV420)
        && (output->v4l2_format != V4L2_PIX_FMT_YVU420))
        ge2d_config->dst_para.canvas_index = output_canvas;

    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.format = (get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420B & (3<<3));;
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.x_rev = 0;
    ge2d_config->dst_para.y_rev = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = output->width;
    ge2d_config->dst_para.height = output->height/2;

    if(current_mirror==1){
        ge2d_config->dst_para.x_rev = 1;
        ge2d_config->dst_para.y_rev = 0;
    }else if(current_mirror==2){
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 1;
    }else{
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
    }

    if(cur_angle==90){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.x_rev ^= 1;
    }else if(cur_angle==180){
        ge2d_config->dst_para.x_rev ^= 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }else if(cur_angle==270){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }

    if(ge2d_context_config_ex(context,ge2d_config)<0) {
        printk("++ge2d configing error.\n");
        return -1;
    }
    stretchblt_noalpha(context,src_left ,src_top/2 ,src_width, src_height/2,output->frame[0],output->frame[1]/2,output->frame[2],output->frame[3]/2);

    /* for cr of  yuv420p or yuv420sp. */
    if((output->v4l2_format==V4L2_PIX_FMT_YUV420)
        ||(output->v4l2_format==V4L2_PIX_FMT_YVU420)){
        /* for cb. */
        canvas_read((output_canvas>>8)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>8)&0xff;
        ge2d_config->dst_para.format=(GE2D_FORMAT_S8_CB|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420B & (3<<3));
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/4;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/4,output->frame[2]/2,output->frame[3]/4);
    }
    /* for cb of yuv420p or yuv420sp. */
    if(output->v4l2_format==V4L2_PIX_FMT_YUV420||
        output->v4l2_format==V4L2_PIX_FMT_YVU420) {
        canvas_read((output_canvas>>16)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>16)&0xff;
        ge2d_config->dst_para.format=(GE2D_FORMAT_S8_CR|GE2D_LITTLE_ENDIAN)|(GE2D_FORMAT_M24_YUV420B & (3<<3));
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/4;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/4,output->frame[2]/2,output->frame[3]/4);
    }


    return output_canvas;
}

int amlvideo3_ge2d_interlace_vdindata_process(vframe_t* vf, ge2d_context_t *context,config_para_ex_t* ge2d_config, struct amlvideo3_output* output)
{
    int src_top ,src_left ,src_width, src_height;
    int dst_top ,dst_left ,dst_width, dst_height;
    canvas_t cs0,cs1,cs2,cd;
    int current_mirror = 0;
    int cur_angle = 0;
    int output_canvas  = output->canvas_id;

    src_top = 0;
    src_left = 0;
    src_width = vf->width;
    src_height = vf->height;

    dst_top = 0;
    dst_left = 0;
    dst_width = output->width;
    dst_height = output->height;

    current_mirror = 0;
    cur_angle = output->angle;
    if(current_mirror == 1)
        cur_angle = (360 - cur_angle%360);
    else
        cur_angle = cur_angle%360;

    if(src_width<src_height)
        cur_angle = (cur_angle+90)%360;

       output_axis_adjust(src_width,src_height,&dst_width,&dst_height,cur_angle);
    dst_top = (output->height-dst_height)/2;
    dst_left = (output->width-dst_width)/2;
    dst_top = dst_top&0xfffffffe;
    dst_left = dst_left&0xfffffffe;
    /* data operating. */

    memset(ge2d_config,0,sizeof(config_para_ex_t));
    if((dst_left!=output->frame[0])||(dst_top!=output->frame[1])
      ||(dst_width!=output->frame[2])||(dst_height!=output->frame[3])){
        ge2d_config->alu_const_color= 0;//0x000000ff;
        ge2d_config->bitmask_en  = 0;
        ge2d_config->src1_gb_alpha = 0;//0xff;
        ge2d_config->dst_xy_swap = 0;

        canvas_read(output_canvas&0xff,&cd);
        ge2d_config->src_planes[0].addr = cd.addr;
        ge2d_config->src_planes[0].w = cd.width;
        ge2d_config->src_planes[0].h = cd.height;
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;

        ge2d_config->src_key.key_enable = 0;
        ge2d_config->src_key.key_mask = 0;
        ge2d_config->src_key.key_mode = 0;

        ge2d_config->src_para.canvas_index=output_canvas;
        ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->src_para.format = get_interlace_input_format(vf, output)|GE2D_LITTLE_ENDIAN;
        ge2d_config->src_para.format = get_interlace_input_format(vf, output);
        ge2d_config->src_para.fill_color_en = 0;
        ge2d_config->src_para.fill_mode = 0;
        ge2d_config->src_para.x_rev = 0;
        ge2d_config->src_para.y_rev = 0;
        ge2d_config->src_para.color = 0;
        ge2d_config->src_para.top = 0;
        ge2d_config->src_para.left = 0;
        ge2d_config->src_para.width = output->width;
        ge2d_config->src_para.height = output->height;

        ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;

        ge2d_config->dst_para.canvas_index=output_canvas;
        ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->dst_para.fill_color_en = 0;
        ge2d_config->dst_para.fill_mode = 0;
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
        ge2d_config->dst_para.color = 0;
        ge2d_config->dst_para.top = 0;
        ge2d_config->dst_para.left = 0;
        ge2d_config->dst_para.width = output->width;
        ge2d_config->dst_para.height = output->height;

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -2;
        }
        fillrect(context, 0, 0, output->width, output->height, (ge2d_config->dst_para.format&GE2D_FORMAT_YUV)?0x008080ff:0);
        output->frame[0] = dst_left;
        output->frame[1] = dst_top;
        output->frame[2] = dst_width;
        output->frame[3] = dst_height;
        memset(ge2d_config,0,sizeof(config_para_ex_t));
    }
    src_height = vf->height/2;
    ge2d_config->alu_const_color= 0;//0x000000ff;
    ge2d_config->bitmask_en  = 0;
    ge2d_config->src1_gb_alpha = 0;//0xff;
    ge2d_config->dst_xy_swap = 0;

    canvas_read(vf->canvas0Addr&0xff,&cs0);
    canvas_read((vf->canvas0Addr>>8)&0xff,&cs1);
    canvas_read((vf->canvas0Addr>>16)&0xff,&cs2);
    ge2d_config->src_planes[0].addr = cs0.addr;
    ge2d_config->src_planes[0].w = cs0.width;
    ge2d_config->src_planes[0].h = cs0.height;
    ge2d_config->src_planes[1].addr = cs1.addr;
    ge2d_config->src_planes[1].w = cs1.width;
    ge2d_config->src_planes[1].h = cs1.height;
    ge2d_config->src_planes[2].addr = cs2.addr;
    ge2d_config->src_planes[2].w = cs2.width;
    ge2d_config->src_planes[2].h = cs2.height;
    canvas_read(output_canvas&0xff,&cd);
    ge2d_config->dst_planes[0].addr = cd.addr;
    ge2d_config->dst_planes[0].w = cd.width;
    ge2d_config->dst_planes[0].h = cd.height;
    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index=vf->canvas0Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_interlace_input_format(vf, output);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    ge2d_config->src_para.height = src_height;
    /* printk("vf_width is %d , vf_height is %d \n",vf->width ,vf->height); */
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.canvas_index = output_canvas&0xff;

    if((output->v4l2_format!= V4L2_PIX_FMT_YUV420)
        && (output->v4l2_format != V4L2_PIX_FMT_YVU420))
        ge2d_config->dst_para.canvas_index = output_canvas;

    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.x_rev = 0;
    ge2d_config->dst_para.y_rev = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = output->width;
    ge2d_config->dst_para.height = output->height;

    if(current_mirror==1){
        ge2d_config->dst_para.x_rev = 1;
        ge2d_config->dst_para.y_rev = 0;
    }else if(current_mirror==2){
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 1;
    }else{
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
    }

    if(cur_angle==90){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.x_rev ^= 1;
    }else if(cur_angle==180){
        ge2d_config->dst_para.x_rev ^= 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }else if(cur_angle==270){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }

    if(ge2d_context_config_ex(context,ge2d_config)<0) {
        printk("++ge2d configing error.\n");
        return -1;
    }
    stretchblt_noalpha(context,src_left ,src_top ,src_width, src_height,output->frame[0],output->frame[1],output->frame[2],output->frame[3]);

    /* for cr of  yuv420p or yuv420sp. */
    if((output->v4l2_format==V4L2_PIX_FMT_YUV420)
        ||(output->v4l2_format==V4L2_PIX_FMT_YVU420)){
        /* for cb. */
        canvas_read((output_canvas>>8)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>8)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CB|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top, src_width, src_height,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    /* for cb of yuv420p or yuv420sp. */
    if(output->v4l2_format==V4L2_PIX_FMT_YUV420||
        output->v4l2_format==V4L2_PIX_FMT_YVU420) {
        canvas_read((output_canvas>>16)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>16)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CR|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top, src_width, src_height,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    return output_canvas;
}

int amlvideo3_ge2d_interlace_one_canvasAddr_process(vframe_t* vf, ge2d_context_t *context,config_para_ex_t* ge2d_config, struct amlvideo3_output* output)
{
    int src_top ,src_left ,src_width, src_height;
    int dst_top ,dst_left ,dst_width, dst_height;
    canvas_t cs0,cs1,cs2,cd;
    int current_mirror = 0;
    int cur_angle = 0;
    int output_canvas  = output->canvas_id;

    src_top = 0;
    src_left = 0;
    src_width = vf->width;
    //src_height = vf->height/2;
    src_height = vf->height;

    dst_top = 0;
    dst_left = 0;
    dst_width = output->width;
    dst_height = output->height;

    current_mirror = 0;
    cur_angle = output->angle;
    if(current_mirror == 1)
        cur_angle = (360 - cur_angle%360);
    else
        cur_angle = cur_angle%360;

    if(src_width<src_height)
        cur_angle = (cur_angle+90)%360;

       output_axis_adjust(src_width,src_height,&dst_width,&dst_height,cur_angle);
    dst_top = (output->height-dst_height)/2;
    dst_left = (output->width-dst_width)/2;
    dst_top = dst_top&0xfffffffe;
    dst_left = dst_left&0xfffffffe;
    /* data operating. */

    memset(ge2d_config,0,sizeof(config_para_ex_t));
    if((dst_left!=output->frame[0])||(dst_top!=output->frame[1])
      ||(dst_width!=output->frame[2])||(dst_height!=output->frame[3])){
        ge2d_config->alu_const_color= 0;//0x000000ff;
        ge2d_config->bitmask_en  = 0;
        ge2d_config->src1_gb_alpha = 0;//0xff;
        ge2d_config->dst_xy_swap = 0;

        canvas_read(output_canvas&0xff,&cd);
        ge2d_config->src_planes[0].addr = cd.addr;
        ge2d_config->src_planes[0].w = cd.width;
        ge2d_config->src_planes[0].h = cd.height;
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;

        ge2d_config->src_key.key_enable = 0;
        ge2d_config->src_key.key_mask = 0;
        ge2d_config->src_key.key_mode = 0;

        ge2d_config->src_para.canvas_index=output_canvas;
        ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->src_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->src_para.fill_color_en = 0;
        ge2d_config->src_para.fill_mode = 0;
        ge2d_config->src_para.x_rev = 0;
        ge2d_config->src_para.y_rev = 0;
        ge2d_config->src_para.color = 0;
        ge2d_config->src_para.top = 0;
        ge2d_config->src_para.left = 0;
        ge2d_config->src_para.width = output->width;
        ge2d_config->src_para.height = output->height;

        ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;

        ge2d_config->dst_para.canvas_index=output_canvas;
        ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->dst_para.fill_color_en = 0;
        ge2d_config->dst_para.fill_mode = 0;
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
        ge2d_config->dst_para.color = 0;
        ge2d_config->dst_para.top = 0;
        ge2d_config->dst_para.left = 0;
        ge2d_config->dst_para.width = output->width;
        ge2d_config->dst_para.height = output->height;

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -2;
        }
        fillrect(context, 0, 0, output->width, output->height, (ge2d_config->dst_para.format&GE2D_FORMAT_YUV)?0x008080ff:0);
        output->frame[0] = dst_left;
        output->frame[1] = dst_top;
        output->frame[2] = dst_width;
        output->frame[3] = dst_height;
        memset(ge2d_config,0,sizeof(config_para_ex_t));
    }
    ge2d_config->alu_const_color= 0;//0x000000ff;
    ge2d_config->bitmask_en  = 0;
    ge2d_config->src1_gb_alpha = 0;//0xff;
    ge2d_config->dst_xy_swap = 0;

    canvas_read(vf->canvas0Addr&0xff,&cs0);
    canvas_read((vf->canvas0Addr>>8)&0xff,&cs1);
    canvas_read((vf->canvas0Addr>>16)&0xff,&cs2);
    ge2d_config->src_planes[0].addr = cs0.addr;
    ge2d_config->src_planes[0].w = cs0.width;
    ge2d_config->src_planes[0].h = cs0.height;
    ge2d_config->src_planes[1].addr = cs1.addr;
    ge2d_config->src_planes[1].w = cs1.width;
    ge2d_config->src_planes[1].h = cs1.height;
    ge2d_config->src_planes[2].addr = cs2.addr;
    ge2d_config->src_planes[2].w = cs2.width;
    ge2d_config->src_planes[2].h = cs2.height;
    canvas_read(output_canvas&0xff,&cd);
    ge2d_config->dst_planes[0].addr = cd.addr;
    ge2d_config->dst_planes[0].w = cd.width;
    ge2d_config->dst_planes[0].h = cd.height;
    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index=vf->canvas0Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_input_format_no_interlace(vf);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    ge2d_config->src_para.height = vf->height/2;
    /* printk("vf_width is %d , vf_height is %d \n",vf->width ,vf->height); */
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.canvas_index = output_canvas&0xff;

    if((output->v4l2_format!= V4L2_PIX_FMT_YUV420)
        && (output->v4l2_format != V4L2_PIX_FMT_YVU420))
        ge2d_config->dst_para.canvas_index = output_canvas;

    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.x_rev = 0;
    ge2d_config->dst_para.y_rev = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = output->width;
    ge2d_config->dst_para.height = output->height;

    if(current_mirror==1){
        ge2d_config->dst_para.x_rev = 1;
        ge2d_config->dst_para.y_rev = 0;
    }else if(current_mirror==2){
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 1;
    }else{
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
    }

    if(cur_angle==90){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.x_rev ^= 1;
    }else if(cur_angle==180){
        ge2d_config->dst_para.x_rev ^= 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }else if(cur_angle==270){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }

    if(ge2d_context_config_ex(context,ge2d_config)<0) {
        printk("++ge2d configing error.\n");
        return -1;
    }
    stretchblt_noalpha(context,src_left ,src_top/2,src_width, src_height/2,output->frame[0],output->frame[1],output->frame[2],output->frame[3]);

    /* for cr of  yuv420p or yuv420sp. */
    if((output->v4l2_format==V4L2_PIX_FMT_YUV420)
        ||(output->v4l2_format==V4L2_PIX_FMT_YVU420)){
        /* for cb. */
        canvas_read((output_canvas>>8)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>8)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CB|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    /* for cb of yuv420p or yuv420sp. */
    if(output->v4l2_format==V4L2_PIX_FMT_YUV420||
        output->v4l2_format==V4L2_PIX_FMT_YVU420) {
        canvas_read((output_canvas>>16)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>16)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CR|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top/2, src_width, src_height/2,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    return output_canvas;
}

int amlvideo3_ge2d_pre_process(vframe_t* vf, ge2d_context_t *context,config_para_ex_t* ge2d_config, struct amlvideo3_output* output)
{
    int src_top ,src_left ,src_width, src_height;
    int dst_top ,dst_left ,dst_width, dst_height;
    canvas_t cs0,cs1,cs2,cd;
    int current_mirror = 0;
    int cur_angle = 0;
    int output_canvas  = output->canvas_id;

    src_top = 0;
    src_left = 0;
    src_width = vf->width;
    src_height = vf->height;

    dst_top = 0;
    dst_left = 0;
    dst_width = output->width;
    dst_height = output->height;

    current_mirror = 0;
    cur_angle = output->angle;
    if(current_mirror == 1)
        cur_angle = (360 - cur_angle%360);
    else
        cur_angle = cur_angle%360;

    if(src_width<src_height)
        cur_angle = (cur_angle+90)%360;

       output_axis_adjust(src_width,src_height,&dst_width,&dst_height,cur_angle);
    dst_top = (output->height-dst_height)/2;
    dst_left = (output->width-dst_width)/2;
    dst_top = dst_top&0xfffffffe;
    dst_left = dst_left&0xfffffffe;
    /* data operating. */

    memset(ge2d_config,0,sizeof(config_para_ex_t));
    if((dst_left!=output->frame[0])||(dst_top!=output->frame[1])
      ||(dst_width!=output->frame[2])||(dst_height!=output->frame[3])){
        ge2d_config->alu_const_color= 0;//0x000000ff;
        ge2d_config->bitmask_en  = 0;
        ge2d_config->src1_gb_alpha = 0;//0xff;
        ge2d_config->dst_xy_swap = 0;

        canvas_read(output_canvas&0xff,&cd);
        ge2d_config->src_planes[0].addr = cd.addr;
        ge2d_config->src_planes[0].w = cd.width;
        ge2d_config->src_planes[0].h = cd.height;
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;

        ge2d_config->src_key.key_enable = 0;
        ge2d_config->src_key.key_mask = 0;
        ge2d_config->src_key.key_mode = 0;

        ge2d_config->src_para.canvas_index=output_canvas;
        ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->src_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->src_para.fill_color_en = 0;
        ge2d_config->src_para.fill_mode = 0;
        ge2d_config->src_para.x_rev = 0;
        ge2d_config->src_para.y_rev = 0;
        ge2d_config->src_para.color = 0;
        ge2d_config->src_para.top = 0;
        ge2d_config->src_para.left = 0;
        ge2d_config->src_para.width = output->width;
        ge2d_config->src_para.height = output->height;

        ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;

        ge2d_config->dst_para.canvas_index=output_canvas;
        ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
        ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;;
        ge2d_config->dst_para.fill_color_en = 0;
        ge2d_config->dst_para.fill_mode = 0;
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
        ge2d_config->dst_para.color = 0;
        ge2d_config->dst_para.top = 0;
        ge2d_config->dst_para.left = 0;
        ge2d_config->dst_para.width = output->width;
        ge2d_config->dst_para.height = output->height;

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -2;
        }
        fillrect(context, 0, 0, output->width, output->height, (ge2d_config->dst_para.format&GE2D_FORMAT_YUV)?0x008080ff:0);
        output->frame[0] = dst_left;
        output->frame[1] = dst_top;
        output->frame[2] = dst_width;
        output->frame[3] = dst_height;
        memset(ge2d_config,0,sizeof(config_para_ex_t));
    }
    ge2d_config->alu_const_color= 0;//0x000000ff;
    ge2d_config->bitmask_en  = 0;
    ge2d_config->src1_gb_alpha = 0;//0xff;
    ge2d_config->dst_xy_swap = 0;

    canvas_read(vf->canvas0Addr&0xff,&cs0);
    canvas_read((vf->canvas0Addr>>8)&0xff,&cs1);
    canvas_read((vf->canvas0Addr>>16)&0xff,&cs2);
    ge2d_config->src_planes[0].addr = cs0.addr;
    ge2d_config->src_planes[0].w = cs0.width;
    ge2d_config->src_planes[0].h = cs0.height;
    ge2d_config->src_planes[1].addr = cs1.addr;
    ge2d_config->src_planes[1].w = cs1.width;
    ge2d_config->src_planes[1].h = cs1.height;
    ge2d_config->src_planes[2].addr = cs2.addr;
    ge2d_config->src_planes[2].w = cs2.width;
    ge2d_config->src_planes[2].h = cs2.height;
    canvas_read(output_canvas&0xff,&cd);
    ge2d_config->dst_planes[0].addr = cd.addr;
    ge2d_config->dst_planes[0].w = cd.width;
    ge2d_config->dst_planes[0].h = cd.height;
    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index=vf->canvas0Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_input_format(vf);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    ge2d_config->src_para.height = vf->height;
    /* printk("vf_width is %d , vf_height is %d \n",vf->width ,vf->height); */
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.canvas_index = output_canvas&0xff;

    if((output->v4l2_format!= V4L2_PIX_FMT_YUV420)
        && (output->v4l2_format != V4L2_PIX_FMT_YVU420))
        ge2d_config->dst_para.canvas_index = output_canvas;

    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.format = get_output_format(output->v4l2_format)|GE2D_LITTLE_ENDIAN;
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.x_rev = 0;
    ge2d_config->dst_para.y_rev = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = output->width;
    ge2d_config->dst_para.height = output->height;

    if(current_mirror==1){
        ge2d_config->dst_para.x_rev = 1;
        ge2d_config->dst_para.y_rev = 0;
    }else if(current_mirror==2){
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 1;
    }else{
        ge2d_config->dst_para.x_rev = 0;
        ge2d_config->dst_para.y_rev = 0;
    }

    if(cur_angle==90){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.x_rev ^= 1;
    }else if(cur_angle==180){
        ge2d_config->dst_para.x_rev ^= 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }else if(cur_angle==270){
        ge2d_config->dst_xy_swap = 1;
        ge2d_config->dst_para.y_rev ^= 1;
    }

    if(ge2d_context_config_ex(context,ge2d_config)<0) {
        printk("++ge2d configing error.\n");
        return -1;
    }
    stretchblt_noalpha(context,src_left ,src_top ,src_width, src_height,output->frame[0],output->frame[1],output->frame[2],output->frame[3]);

    /* for cr of  yuv420p or yuv420sp. */
    if((output->v4l2_format==V4L2_PIX_FMT_YUV420)
        ||(output->v4l2_format==V4L2_PIX_FMT_YVU420)){
        /* for cb. */
        canvas_read((output_canvas>>8)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>8)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CB|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top, src_width, src_height,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    /* for cb of yuv420p or yuv420sp. */
    if(output->v4l2_format==V4L2_PIX_FMT_YUV420||
        output->v4l2_format==V4L2_PIX_FMT_YVU420) {
        canvas_read((output_canvas>>16)&0xff,&cd);
        ge2d_config->dst_planes[0].addr = cd.addr;
        ge2d_config->dst_planes[0].w = cd.width;
        ge2d_config->dst_planes[0].h = cd.height;
        ge2d_config->dst_para.canvas_index=(output_canvas>>16)&0xff;
        ge2d_config->dst_para.format=GE2D_FORMAT_S8_CR|GE2D_LITTLE_ENDIAN;
        ge2d_config->dst_para.width = output->width/2;
        ge2d_config->dst_para.height = output->height/2;
        ge2d_config->dst_xy_swap = 0;

        if(current_mirror==1){
            ge2d_config->dst_para.x_rev = 1;
            ge2d_config->dst_para.y_rev = 0;
        }else if(current_mirror==2){
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 1;
        }else{
            ge2d_config->dst_para.x_rev = 0;
            ge2d_config->dst_para.y_rev = 0;
        }

        if(cur_angle==90){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.x_rev ^= 1;
        }else if(cur_angle==180){
            ge2d_config->dst_para.x_rev ^= 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }else if(cur_angle==270){
            ge2d_config->dst_xy_swap = 1;
            ge2d_config->dst_para.y_rev ^= 1;
        }

        if(ge2d_context_config_ex(context,ge2d_config)<0) {
            printk("++ge2d configing error.\n");
            return -1;
        }
        stretchblt_noalpha(context, src_left, src_top, src_width, src_height,
            output->frame[0]/2,output->frame[1]/2,output->frame[2]/2,output->frame[3]/2);
    }
    return output_canvas;
}

static inline void ge2d_src_config(struct vframe_s* vf, config_para_ex_t* ge2d_config) {
    canvas_t src_cs0, src_cs1, src_cs2;

    /* data operating. */
    ge2d_config->alu_const_color = 0; //0x000000ff;
    ge2d_config->bitmask_en = 0;
    ge2d_config->src1_gb_alpha = 0; //0xff;

    canvas_read(vf->canvas0Addr & 0xff, &src_cs0);
    canvas_read(vf->canvas0Addr >> 8 & 0xff, &src_cs1);
    canvas_read(vf->canvas0Addr >> 16 & 0xff, &src_cs2);
    ge2d_config->src_planes[0].addr = src_cs0.addr;
    ge2d_config->src_planes[0].w = src_cs0.width;
    ge2d_config->src_planes[0].h = src_cs0.height;
    ge2d_config->src_planes[1].addr = src_cs1.addr;
    ge2d_config->src_planes[1].w = src_cs1.width;
    ge2d_config->src_planes[1].h = src_cs1.height;
    ge2d_config->src_planes[2].addr = src_cs2.addr;
    ge2d_config->src_planes[2].w = src_cs2.width;
    ge2d_config->src_planes[2].h = src_cs2.height;

    ge2d_config->src_key.key_enable = 0;
    ge2d_config->src_key.key_mask = 0;
    ge2d_config->src_key.key_mode = 0;
    ge2d_config->src_para.canvas_index = vf->canvas0Addr;
    ge2d_config->src_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->src_para.format = get_input_format_no_interlace(vf);
    ge2d_config->src_para.fill_color_en = 0;
    ge2d_config->src_para.fill_mode = 0;
    ge2d_config->src_para.x_rev = 0;
    ge2d_config->src_para.y_rev = 0;
    ge2d_config->src_para.color = 0xffffffff;
    ge2d_config->src_para.top = 0;
    ge2d_config->src_para.left = 0;
    ge2d_config->src_para.width = vf->width;
    int height = (deinterlaced || (vf->type & VIDTYPE_INTERLACE_BOTTOM) || (vf->type & VIDTYPE_INTERLACE_TOP)) ? (vf->height >> 1) : vf->height;
    ge2d_config->src_para.height = height;
    ge2d_config->src2_para.mem_type = CANVAS_TYPE_INVALID;
    ppmgr3_printk(2, "vf_width is %d , vf_height is %d type:%p\n", vf->width, vf->height, (void *)vf->type);
}

/*
 * use ppmgr3 need to init ge2d_context_t, pixel_format, canvas_width, canvas_height,
 * phy_addr, buffer_size, canvas_number.
 */
static int ppmgr3_init(struct ppmgr3_device *ppd) {
    int i = 0;
    switch_mod_gate_by_name("ge2d", 1);
    ppd->context = create_ge2d_work_queue();
    if (!ppd->context) {
        ppmgr3_printk(1, "create ge2d work queue error!\n");
        return -1;
    }
    ppmgr3_printk(2, "ppmgr3_init!\n");
    ppd->paint_mode = 0;
    ppd->angle = 0;
    ppd->mirror = 0;
    ppd->ge2d_fmt = 0;
    ppd->dst_width = 0;
    ppd->dst_height = 0;
    for (i = 0; i < PPMGR3_MAX_CANVAS; i++) {
        ppd->phy_addr[i] = NULL;
        ppd->canvas_id[i] = -1;
    }
    return 0;
}

static int ppmgr3_canvas_config(struct ppmgr3_device *ppd, int dst_width, int dst_height, int dst_fmt, void* phy_addr, int index) {
    int canvas_width = ALIGN(dst_width, 32);
    int canvas_height = dst_height;

    if (!ppd->phy_addr) {
        ppmgr3_printk(1, "NULL physical address!\n");
        return -1;
    }
    ppd->ge2d_fmt = v4l_to_ge2d_format(dst_fmt);
    ppd->dst_width = dst_width;
    ppd->dst_height = dst_height;
    ppd->phy_addr[index] = phy_addr;

    if (ppd->ge2d_fmt == GE2D_FORMAT_M24_NV21 || ppd->ge2d_fmt == GE2D_FORMAT_M24_NV12) {
        canvas_config(PPMGR3_CANVAS_INDEX + index * 2, (ulong) phy_addr, canvas_width, canvas_height, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        canvas_config(PPMGR3_CANVAS_INDEX + index * 2 + 1, (ulong)(phy_addr + (canvas_width * canvas_height)), canvas_width, canvas_height >> 1, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        ppd->canvas_id[index] = (PPMGR3_CANVAS_INDEX + index * 2) | ((PPMGR3_CANVAS_INDEX + index * 2 + 1) << 8);
    } else if (ppd->ge2d_fmt == GE2D_FORMAT_S8_Y) {
        canvas_config(PPMGR3_CANVAS_INDEX + index * 3, (ulong) phy_addr, canvas_width, canvas_height, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        canvas_config(PPMGR3_CANVAS_INDEX + index * 3 + 1, (ulong)(phy_addr + canvas_width * canvas_height), canvas_width >> 1, canvas_height >> 1, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        canvas_config(PPMGR3_CANVAS_INDEX + index * 3 + 2, (ulong)(phy_addr + (canvas_width * canvas_height * 5 >> 2)), canvas_width >> 1, canvas_height >> 1, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        ppd->canvas_id[index] = (PPMGR3_CANVAS_INDEX + index * 3) | ((PPMGR3_CANVAS_INDEX + index * 3 + 1) << 8) | ((PPMGR3_CANVAS_INDEX + index * 3 + 2) << 16);
    } else {
        int bpp = 0;
        if (ppd->ge2d_fmt == GE2D_FORMAT_S32_ABGR) {
            bpp = 4;
        } else if (ppd->ge2d_fmt == GE2D_FORMAT_S24_BGR || ppd->ge2d_fmt == GE2D_FORMAT_S24_RGB) {
            bpp = 3;
        } else if (ppd->ge2d_fmt == GE2D_FORMAT_S16_RGB_565) {
            bpp = 2;
        } else {
            ppmgr3_printk(1, "Not support format!\n");
            return -1;
        }
        canvas_config(PPMGR3_CANVAS_INDEX + index, (ulong)phy_addr, canvas_width * bpp, canvas_height, CANVAS_ADDR_NOWRAP, CANVAS_BLKMODE_LINEAR);
        ppd->canvas_id[index] = PPMGR3_CANVAS_INDEX + index;

    }
    ppmgr3_printk(2, "canvas[%d] phy_addr:%p width:%d height:%d\n", index, phy_addr, canvas_width, canvas_height);

    return 0;
}

static int ppmgr3_process(struct vframe_s* vf, struct ppmgr3_device *ppd, int index) {
    int ret = 0;
    int dst_canvas_id = ppd->canvas_id[index];
    int dst_pixel_format = ppd->ge2d_fmt;
    ge2d_context_t *context = ppd->context;
    config_para_ex_t* ge2d_config = &(ppd->ge2d_config);

    if(((vf->type &VIDTYPE_TYPEMASK) == VIDTYPE_INTERLACE_BOTTOM)&&(vf->canvas0Addr == vf->canvas1Addr)){
        // skip
        return -1;
    }

    ge2d_src_config(vf, ge2d_config);

    canvas_t dst_cd;
    ge2d_config->dst_para.mem_type = CANVAS_TYPE_INVALID;
    ge2d_config->dst_para.fill_color_en = 0;
    ge2d_config->dst_para.fill_mode = 0;
    ge2d_config->dst_para.color = 0;
    ge2d_config->dst_para.top = 0;
    ge2d_config->dst_para.left = 0;
    ge2d_config->dst_para.width = ppd->dst_width;
    ge2d_config->dst_para.height = ppd->dst_height;

    canvas_read(dst_canvas_id & 0xff, &dst_cd);
    ge2d_config->dst_planes[0].addr = dst_cd.addr;
    ge2d_config->dst_planes[0].w = dst_cd.width;
    ge2d_config->dst_planes[0].h = dst_cd.height;
    ge2d_config->dst_para.format = dst_pixel_format | GE2D_LITTLE_ENDIAN;
    ge2d_config->dst_para.canvas_index = dst_canvas_id;

    if (ge2d_context_config_ex(context, ge2d_config) < 0) {
        return -1;
    }

    int height = (deinterlaced || (vf->type & VIDTYPE_INTERLACE_BOTTOM) || (vf->type & VIDTYPE_INTERLACE_TOP)) ? (vf->height >> 1) : vf->height;
    stretchblt_noalpha(context, 0, 0, vf->width, height, 0, 0, ppd->dst_width, ppd->dst_height);

    // if( (vf->type &VIDTYPE_INTERLACE_BOTTOM) || (vf->type &VIDTYPE_INTERLACE_TOP)){
    //     if(vf->canvas0Addr == vf->canvas1Addr){
    //         amlvideo3_ge2d_interlace_one_canvasAddr_process(vf,context,ge2d_config,&output);
    //     }else{
    //         amlvideo3_ge2d_interlace_two_canvasAddr_process(vf,context,ge2d_config,&output);
    //     }
    // }else{
    //     amlvideo3_ge2d_pre_process(vf,context,ge2d_config,&output);
    // }

    return 0;
}

static void ppmgr3_release(struct ppmgr3_device *ppd) {
    if (ppd->context) {
        destroy_ge2d_work_queue(ppd->context);
    }
    switch_mod_gate_by_name("ge2d", 0);
    ppmgr3_printk(2, "ppmgr3_release!\n");
}

static const struct ionvideo2_fmt formats[] = {
    {
        .name     = "12  Y/CrCb 4:2:0",
        .fourcc   = V4L2_PIX_FMT_NV21,
        .depth    = 12,
    }
};

static const struct ionvideo2_fmt *__get_format(u32 pixelformat)
{
    const struct ionvideo2_fmt *fmt;
    unsigned int k;

    for (k = 0; k < ARRAY_SIZE(formats); k++) {
        fmt = &formats[k];
        if (fmt->fourcc == pixelformat)
            break;
    }

    if (k == ARRAY_SIZE(formats))
        return NULL;

    return &formats[k];
}

static const struct ionvideo2_fmt *get_format(struct v4l2_format *f)
{
    return __get_format(f->fmt.pix.pixelformat);
}

static LIST_HEAD (ionvideo2_devlist);

static DEFINE_SPINLOCK(ion_states_lock);
static int  ionvideo2_vf_get_states(vframe_states_t *states)
{
    int ret = -1;
    unsigned long flags;
    struct vframe_provider_s *vfp;
    vfp = vf_get_provider(RECEIVER_NAME);
    spin_lock_irqsave(&ion_states_lock, flags);
    if (vfp && vfp->ops && vfp->ops->vf_states) {
        ret=vfp->ops->vf_states(states, vfp->op_arg);
    }
    spin_unlock_irqrestore(&ion_states_lock, flags);
    return ret;
}

/* ------------------------------------------------------------------
 DMA and thread functions
 ------------------------------------------------------------------*/
static unsigned get_ionvideo2_debug(void) {
    return debug;
}
EXPORT_SYMBOL(get_ionvideo2_debug);

int is_ionvideo2_active(void) {
    return is_actived;
}
EXPORT_SYMBOL(is_ionvideo2_active);

static int ionvideo2_fillbuff(struct ionvideo2_dev *dev, struct ionvideo2_buffer *buf) {

    struct vframe_s* vf;
    struct vb2_buffer *vb = &(buf->vb);
    int ret = 0;
//-------------------------------------------------------
    vf = vf_get(RECEIVER_NAME);
    if (!vf) {
        return -EAGAIN;
    }

    dev->pts = vf->pts_us64;

    ret = ppmgr3_process(vf, &dev->ppmgr3_dev, vb->v4l2_buf.index);
    if (ret) {
        vf_put(vf, RECEIVER_NAME);
        return ret;
    }
    vf_put(vf, RECEIVER_NAME);
    buf->vb.v4l2_buf.timestamp.tv_sec = dev->pts >> 32;
    buf->vb.v4l2_buf.timestamp.tv_usec = dev->pts & 0xFFFFFFFF;
//-------------------------------------------------------
    return 0;
}

static int ionvideo2_size_changed(struct ionvideo2_dev *dev, struct vframe_s* vf) {
    int aw = vf->width;
    int ah = vf->height;

    v4l_bound_align_image(&aw, 48, MAX_WIDTH, 5, &ah, 32, MAX_HEIGHT, 0, 0);
    dev->c_width = aw;
    dev->c_height = ah;
    if (aw != dev->width || ah != dev->height) {
        dprintk(dev, 2, "Video frame size changed w:%d h:%d\n", aw, ah);
        return -EAGAIN;
    }
    return 0;
}

static void ionvideo2_thread_tick(struct ionvideo2_dev *dev) {
    struct ionvideo2_dmaqueue *dma_q = &dev->vidq;
    struct ionvideo2_buffer *buf;
    unsigned long flags = 0;
    struct vframe_s* vf;

    dprintk(dev, 4, "Thread tick\n");
    /* video seekTo clear list */

    vf = vf_peek(RECEIVER_NAME);
    if (!vf) {
        msleep(5);
        return;
    }
    spin_lock_irqsave(&dev->slock, flags);
    if (list_empty(&dma_q->active)) {
        dprintk(dev, 3, "No active queue to serve\n");
        spin_unlock_irqrestore(&dev->slock, flags);
        msleep(5);
        return;
    }
    buf = list_entry(dma_q->active.next, struct ionvideo2_buffer, list);
    spin_unlock_irqrestore(&dev->slock, flags);
    /* Fill buffer */
    if (ionvideo2_fillbuff(dev, buf)) {
        return;
    }
    spin_lock_irqsave(&dev->slock, flags);
    list_del(&buf->list);
    spin_unlock_irqrestore(&dev->slock, flags);
    vb2_buffer_done(&buf->vb, VB2_BUF_STATE_DONE);
    dprintk(dev, 4, "[%p/%d] done\n", buf, buf->vb.v4l2_buf.index);
}

static void ionvideo2_sleep(struct ionvideo2_dev *dev) {
    struct ionvideo2_dmaqueue *dma_q = &dev->vidq;
    //int timeout;
    DECLARE_WAITQUEUE(wait, current);

    dprintk(dev, 4, "%s dma_q=0x%08lx\n", __func__, (unsigned long)dma_q);

    add_wait_queue(&dma_q->wq, &wait);
    if (kthread_should_stop())
        goto stop_task;

    ionvideo2_thread_tick(dev);

stop_task:
    remove_wait_queue(&dma_q->wq, &wait);
    try_to_freeze();
}

static int ionvideo2_thread(void *data) {
    struct ionvideo2_dev *dev = data;

    dprintk(dev, 2, "thread started\n");

    set_freezable();

    for (;;) {
        ionvideo2_sleep(dev);

        if (kthread_should_stop())
            break;
    }
    dprintk(dev, 2, "thread: exit\n");
    return 0;
}

static int ionvideo2_start_generating(struct ionvideo2_dev *dev) {
    struct ionvideo2_dmaqueue *dma_q = &dev->vidq;

    dprintk(dev, 2, "%s\n", __func__);

    /* Resets frame counters */
    dev->ms = 0;
    //dev->jiffies = jiffies;

    dma_q->frame = 0;
    //dma_q->ini_jiffies = jiffies;
    dma_q->kthread = kthread_run(ionvideo2_thread, dev, dev->v4l2_dev.name);

    if (IS_ERR(dma_q->kthread)) {
        v4l2_err(&dev->v4l2_dev, "kernel_thread() failed\n");
        return PTR_ERR(dma_q->kthread);
    }
    /* Wakes thread */
    wake_up_interruptible(&dma_q->wq);

    dprintk(dev, 2, "returning from %s\n", __func__);
    return 0;
}

static void ionvideo2_stop_generating(struct ionvideo2_dev *dev) {
    struct ionvideo2_dmaqueue *dma_q = &dev->vidq;

    dprintk(dev, 2, "%s\n", __func__);

    /* shutdown control thread */
    if (dma_q->kthread) {
        kthread_stop(dma_q->kthread);
        dma_q->kthread = NULL;
    }

    /*
     * Typical driver might need to wait here until dma engine stops.
     * In this case we can abort imiedetly, so it's just a noop.
     */

    /* Release all active buffers */
    while (!list_empty(&dma_q->active)) {
        struct ionvideo2_buffer *buf;
        buf = list_entry(dma_q->active.next, struct ionvideo2_buffer, list);
        list_del(&buf->list);
        vb2_buffer_done(&buf->vb, VB2_BUF_STATE_ERROR);
        dprintk(dev, 2, "[%p/%d] done\n", buf, buf->vb.v4l2_buf.index);
    }
}
/* ------------------------------------------------------------------
 Videobuf operations
 ------------------------------------------------------------------*/
static int queue_setup(struct vb2_queue *vq, const struct v4l2_format *fmt, unsigned int *nbuffers, unsigned int *nplanes, unsigned int sizes[], void *alloc_ctxs[]) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vq);
    unsigned long size;

    if (fmt)
        size = fmt->fmt.pix.sizeimage;
    else
        size = (dev->width * dev->height * dev->pixelsize) >> 3;

    if (size == 0)
        return -EINVAL;

    if (0 == *nbuffers)
        *nbuffers = 32;

    while (size * *nbuffers > vid_limit * MAX_WIDTH * MAX_HEIGHT)
        (*nbuffers)--;

    *nplanes = 1;

    sizes[0] = size;

    /*
     * videobuf2-vmalloc allocator is context-less so no need to set
     * alloc_ctxs array.
     */

    dprintk(dev, 2, "%s, count=%d, size=%ld\n", __func__, *nbuffers, size);

    return 0;
}

static int buffer_prepare(struct vb2_buffer *vb) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vb->vb2_queue);
    struct ionvideo2_buffer *buf = container_of(vb, struct ionvideo2_buffer, vb);
    unsigned long size;

    dprintk(dev, 2, "%s, field=%d\n", __func__, vb->v4l2_buf.field);

    BUG_ON(NULL == dev->fmt);

    /*
     * Theses properties only change when queue is idle, see s_fmt.
     * The below checks should not be performed here, on each
     * buffer_prepare (i.e. on each qbuf). Most of the code in this function
     * should thus be moved to buffer_init and s_fmt.
     */
    if (dev->width < 48 || dev->width > MAX_WIDTH || dev->height < 32 || dev->height > MAX_HEIGHT)
        return -EINVAL;

    size = (dev->width * dev->height * dev->pixelsize) >> 3;
    if (vb2_plane_size(vb, 0) < size) {
        dprintk(dev, 1, "%s data will not fit into plane (%lu < %lu)\n", __func__, vb2_plane_size(vb, 0), size);
        return -EINVAL;
    }

    vb2_set_plane_payload(&buf->vb, 0, size);

    buf->fmt = dev->fmt;

    return 0;
}

static void buffer_queue(struct vb2_buffer *vb) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vb->vb2_queue);
    struct ionvideo2_buffer *buf = container_of(vb, struct ionvideo2_buffer, vb);
    struct ionvideo2_dmaqueue *vidq = &dev->vidq;
    unsigned long flags = 0;

    dprintk(dev, 2, "%s\n", __func__);

    spin_lock_irqsave(&dev->slock, flags);
    list_add_tail(&buf->list, &vidq->active);
    spin_unlock_irqrestore(&dev->slock, flags);
}

static int start_streaming(struct vb2_queue *vq, unsigned int count) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vq);
    is_actived = 1;
    dprintk(dev, 2, "%s\n", __func__);
    return ionvideo2_start_generating(dev);
}

/* abort streaming and wait for last buffer */
static int stop_streaming(struct vb2_queue *vq) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vq);
    is_actived = 0;
    dprintk(dev, 2, "%s\n", __func__);
    ionvideo2_stop_generating(dev);
    return 0;
}

static void ionvideo2_lock(struct vb2_queue *vq) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vq);
    mutex_lock(&dev->mutex);
}

static void ionvideo2_unlock(struct vb2_queue *vq) {
    struct ionvideo2_dev *dev = vb2_get_drv_priv(vq);
    mutex_unlock(&dev->mutex);
}

static const struct vb2_ops ionvideo2_video_qops = {
    .queue_setup = queue_setup,
    .buf_prepare = buffer_prepare,
    .buf_queue = buffer_queue,
    .start_streaming = start_streaming,
    .stop_streaming = stop_streaming,
    .wait_prepare = ionvideo2_unlock,
    .wait_finish = ionvideo2_lock,
};

/* ------------------------------------------------------------------
 IOCTL vidioc handling
 ------------------------------------------------------------------*/
static int vidioc_open(struct file *file) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    if (dev->fd_num > 0 || ppmgr3_init(&(dev->ppmgr3_dev)) < 0) {
        return -EBUSY;
    }
    dev->fd_num++;
    dev->pts = 0;
    dev->c_width = 0;
    dev->c_height = 0;
    skip_frames = 0;
    dprintk(dev, 2, "vidioc_open\n");
    printk("ionvideo2 open\n");
    return v4l2_fh_open(file);
}

static int vidioc_release(struct file *file) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    ionvideo2_stop_generating(dev);
    printk("ionvideo2_stop_generating!!!!\n");
    ppmgr3_release(&(dev->ppmgr3_dev));
    dprintk(dev, 2, "vidioc_release\n");
    printk("ionvideo2 release\n");
    if (dev->fd_num > 0) {
        dev->fd_num--;
    }
    return vb2_fop_release(file);
}

static int vidioc_querycap(struct file *file, void *priv, struct v4l2_capability *cap) {
    struct ionvideo2_dev *dev = video_drvdata(file);

    strcpy(cap->driver, "ionvideo2");
    strcpy(cap->card, "ionvideo2");
    snprintf(cap->bus_info, sizeof(cap->bus_info), "platform:%s", dev->v4l2_dev.name);
    cap->device_caps = V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING | V4L2_CAP_READWRITE;
    cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;
    return 0;
}

static int vidioc_enum_fmt_vid_cap(struct file *file, void *priv, struct v4l2_fmtdesc *f) {
    const struct ionvideo2_fmt *fmt;

    if (f->index >= ARRAY_SIZE(formats))
        return -EINVAL;

    fmt = &formats[f->index];

    strlcpy(f->description, fmt->name, sizeof(f->description));
    f->pixelformat = fmt->fourcc;
    return 0;
}

static int vidioc_g_fmt_vid_cap(struct file *file, void *priv, struct v4l2_format *f) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    struct vb2_queue *q = &dev->vb_vidq;
    int ret = 0;
    unsigned long flags;

    f->fmt.pix.width = dev->width;
    f->fmt.pix.height = dev->height;
    f->fmt.pix.field = V4L2_FIELD_INTERLACED;
    f->fmt.pix.pixelformat = dev->fmt->fourcc;
    f->fmt.pix.bytesperline = (f->fmt.pix.width * dev->fmt->depth) >> 3;
    f->fmt.pix.sizeimage = f->fmt.pix.height * f->fmt.pix.bytesperline;
    if (dev->fmt->is_yuv)
        f->fmt.pix.colorspace = V4L2_COLORSPACE_SMPTE170M;
    else
        f->fmt.pix.colorspace = V4L2_COLORSPACE_SRGB;

    return 0;
}

static int vidioc_try_fmt_vid_cap(struct file *file, void *priv, struct v4l2_format *f) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    const struct ionvideo2_fmt *fmt;

    fmt = get_format(f);
    if (!fmt) {
        dprintk(dev, 1, "Fourcc format (0x%08x) unknown.\n", f->fmt.pix.pixelformat);
        return -EINVAL;
    }

    f->fmt.pix.field = V4L2_FIELD_INTERLACED;
    v4l_bound_align_image(&f->fmt.pix.width, 48, MAX_WIDTH, 4, &f->fmt.pix.height, 32, MAX_HEIGHT, 0, 0);
    f->fmt.pix.bytesperline = (f->fmt.pix.width * fmt->depth) >> 3;
    f->fmt.pix.sizeimage = f->fmt.pix.height * f->fmt.pix.bytesperline;
    if (fmt->is_yuv)
        f->fmt.pix.colorspace = V4L2_COLORSPACE_SMPTE170M;
    else
        f->fmt.pix.colorspace = V4L2_COLORSPACE_SRGB;
    f->fmt.pix.priv = 0;
    return 0;
}

static int vidioc_s_fmt_vid_cap(struct file *file, void *priv, struct v4l2_format *f) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    struct vb2_queue *q = &dev->vb_vidq;

    int ret = vidioc_try_fmt_vid_cap(file, priv, f);
    if (ret < 0)
        return ret;

    if (vb2_is_busy(q)) {
        dprintk(dev, 1, "%s device busy\n", __func__);
        return -EBUSY;
    }
    dev->fmt = get_format(f);
    dev->pixelsize = dev->fmt->depth;
    dev->width = f->fmt.pix.width;
    dev->height = f->fmt.pix.height;

    return 0;
}

static int vidioc_enum_framesizes(struct file *file, void *fh, struct v4l2_frmsizeenum *fsize) {
    static const struct v4l2_frmsize_stepwise sizes = { 48, MAX_WIDTH, 4, 32, MAX_HEIGHT, 1 };
    int i;

    if (fsize->index)
        return -EINVAL;
    for (i = 0; i < ARRAY_SIZE(formats); i++)
        if (formats[i].fourcc == fsize->pixel_format)
            break;
    if (i == ARRAY_SIZE(formats))
        return -EINVAL;
    fsize->type = V4L2_FRMSIZE_TYPE_STEPWISE;
    fsize->stepwise = sizes;
    return 0;
}

static int vidioc_qbuf(struct file *file, void *priv, struct v4l2_buffer *p) {
    struct ionvideo2_dev *dev = video_drvdata(file);
    struct ppmgr3_device* ppmgr3_dev = &(dev->ppmgr3_dev);
    int ret = 0;

    ret = vb2_ioctl_qbuf(file, priv, p);
    if (ret != 0) { return ret; }

    if (!ppmgr3_dev->phy_addr[p->index]){
        struct vb2_buffer *vb;
        struct vb2_queue *q;
        void* phy_addr = NULL;
        q = dev->vdev.queue;
        vb = q->bufs[p->index];
        phy_addr = vb2_plane_cookie(vb, 0);
        if (phy_addr) {
            ret = ppmgr3_canvas_config(ppmgr3_dev, dev->width, dev->height, dev->fmt->fourcc, phy_addr, p->index);
        } else {
            return -ENOMEM;
        }
    }

    return ret;
}

#define NUM_INPUTS 10
/* only one input in this sample driver */
static int vidioc_enum_input(struct file *file, void *priv, struct v4l2_input *inp) {
    if (inp->index >= NUM_INPUTS)
        return -EINVAL;

    inp->type = V4L2_INPUT_TYPE_CAMERA;
    sprintf(inp->name, "Camera %u", inp->index);
    return 0;
}

static int vidioc_g_input(struct file *file, void *priv, unsigned int *i) {
    struct ionvideo2_dev *dev = video_drvdata(file);

    *i = dev->input;
    return 0;
}

static int vidioc_s_input(struct file *file, void *priv, unsigned int i) {
    struct ionvideo2_dev *dev = video_drvdata(file);

    if (i >= NUM_INPUTS)
        return -EINVAL;

    if (i == dev->input)
        return 0;

    dev->input = i;
    return 0;
}

/* ------------------------------------------------------------------
 File operations for the device
 ------------------------------------------------------------------*/
static const struct v4l2_file_operations ionvideo2_fops = {
    .owner = THIS_MODULE,
    .open = vidioc_open,
    .release = vidioc_release,
    .read = vb2_fop_read,
    .poll = vb2_fop_poll,
    .unlocked_ioctl = video_ioctl2, /* V4L2 ioctl handler */
    .mmap = vb2_fop_mmap,
};

static const struct v4l2_ioctl_ops ionvideo2_ioctl_ops = {
    .vidioc_querycap = vidioc_querycap,
    .vidioc_enum_fmt_vid_cap = vidioc_enum_fmt_vid_cap,
    .vidioc_g_fmt_vid_cap = vidioc_g_fmt_vid_cap,
    .vidioc_try_fmt_vid_cap = vidioc_try_fmt_vid_cap,
    .vidioc_s_fmt_vid_cap = vidioc_s_fmt_vid_cap,
    .vidioc_enum_framesizes = vidioc_enum_framesizes,
    .vidioc_reqbufs = vb2_ioctl_reqbufs,
    .vidioc_create_bufs = vb2_ioctl_create_bufs,
    .vidioc_prepare_buf = vb2_ioctl_prepare_buf,
    .vidioc_querybuf = vb2_ioctl_querybuf,
    .vidioc_qbuf = vidioc_qbuf,
    .vidioc_dqbuf = vb2_ioctl_dqbuf,
    .vidioc_enum_input = vidioc_enum_input,
    .vidioc_g_input = vidioc_g_input,
    .vidioc_s_input = vidioc_s_input,
    .vidioc_streamon = vb2_ioctl_streamon,
    .vidioc_streamoff = vb2_ioctl_streamoff,
    .vidioc_log_status = v4l2_ctrl_log_status,
    .vidioc_subscribe_event = v4l2_ctrl_subscribe_event,
    .vidioc_unsubscribe_event = v4l2_event_unsubscribe,
};

static const struct video_device ionvideo2_template = {
    .name = "ionvideo2",
    .fops = &ionvideo2_fops,
    .ioctl_ops = &ionvideo2_ioctl_ops,
    .release = video_device_release_empty,
};

/* -----------------------------------------------------------------
 Initialization and module stuff
 ------------------------------------------------------------------*/
//struct vb2_dc_conf * ionvideo2_dma_ctx = NULL;
static int ionvideo2_release(void) {
    struct ionvideo2_dev *dev;
    struct list_head *list;

    while (!list_empty(&ionvideo2_devlist)) {
        list = ionvideo2_devlist.next;
        list_del(list);
        dev = list_entry(list, struct ionvideo2_dev, ionvideo2_devlist);

        v4l2_info(&dev->v4l2_dev, "unregistering %s\n", video_device_node_name(&dev->vdev));
        video_unregister_device(&dev->vdev);
        v4l2_device_unregister(&dev->v4l2_dev);
        kfree(dev);
    }
    //vb2_dma_contig_cleanup_ctx(ionvideo2_dma_ctx);

    return 0;
}

static int video_receiver_event_fun(int type, void* data, void* private_data) {

    struct ionvideo2_dev *dev = (struct ionvideo2_dev *)private_data;

    if (type == VFRAME_EVENT_PROVIDER_UNREG) {
        dev->receiver_register = 0;
        tsync_avevent(VIDEO_STOP, 0);
        printk("unreg:ionvideo2\n");
    }else if (type == VFRAME_EVENT_PROVIDER_REG) {
        dev->receiver_register = 1;
        dev->ppmgr3_dev.interlaced_num = 0;
        printk("reg:ionvideo2\n");
    }else if (type == VFRAME_EVENT_PROVIDER_QUREY_STATE) {
        return RECEIVER_ACTIVE ;
    }
    return 0;
}

static const struct vframe_receiver_op_s video_vf_receiver = { .event_cb = video_receiver_event_fun };

static int __init ionvideo2_create_instance()
{
    struct ionvideo2_dev *dev;
    struct video_device *vfd;
    struct vb2_queue *q;
    int ret;

    dev = kzalloc(sizeof(*dev), GFP_KERNEL);
    if (!dev)
    return -ENOMEM;

    snprintf(dev->v4l2_dev.name, sizeof(dev->v4l2_dev.name), "%s", IONVIDEO_MODULE_NAME);
    ret = v4l2_device_register(NULL, &dev->v4l2_dev);
    if (ret)
    goto free_dev;

    dev->fmt = &formats[0];
    dev->width = 640;
    dev->height = 480;
    dev->pixelsize = dev->fmt->depth;
    dev->fd_num = 0;

    /* initialize locks */
    spin_lock_init(&dev->slock);

    /* initialize queue */
    q = &dev->vb_vidq;
    q->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    q->io_modes = VB2_MMAP | VB2_USERPTR | VB2_DMABUF | VB2_READ;
    q->drv_priv = dev;
    q->buf_struct_size = sizeof(struct ionvideo2_buffer);
    q->ops = &ionvideo2_video_qops;
    q->mem_ops = &vb2_ion_memops;
    q->timestamp_type = V4L2_BUF_FLAG_TIMESTAMP_MONOTONIC;

    ret = vb2_queue_init(q);
    if (ret)
    goto unreg_dev;

    mutex_init(&dev->mutex);

    /* init video dma queues */
    INIT_LIST_HEAD(&dev->vidq.active);
    init_waitqueue_head(&dev->vidq.wq);

    vfd = &dev->vdev;
    *vfd = ionvideo2_template;
    vfd->debug = debug;
    vfd->v4l2_dev = &dev->v4l2_dev;
    vfd->queue = q;
    set_bit(V4L2_FL_USE_FH_PRIO, &vfd->flags);

    /*
     * Provide a mutex to v4l2 core. It will be used to protect
     * all fops and v4l2 ioctls.
     */
    vfd->lock = &dev->mutex;
    video_set_drvdata(vfd, dev);

    ret = video_register_device(vfd, VFL_TYPE_GRABBER, video_nr);
    if (ret < 0)
        goto unreg_dev;

    /* Now that everything is fine, let's add it to device list */
    list_add_tail(&dev->ionvideo2_devlist, &ionvideo2_devlist);
    vf_receiver_init(&dev->video_vf_receiver, RECEIVER_NAME, &video_vf_receiver, dev);
    vf_reg_receiver(&dev->video_vf_receiver);
    v4l2_info(&dev->v4l2_dev, "V4L2 device registered as %s\n",
            video_device_node_name(vfd));
    return 0;

unreg_dev:
    v4l2_device_unregister(&dev->v4l2_dev);
free_dev:
    kfree(dev);
    return ret;
}

static struct class_attribute ion_video_class_attrs[] = {
    __ATTR_NULL
};
static struct class ionvideo2_class = {
    .name = "ionvideo2",
    .class_attrs = ion_video_class_attrs,
};

/* This routine allocates from 1 to n_devs virtual drivers.

 The real maximum number of virtual drivers will depend on how many drivers
 will succeed. This is limited to the maximum number of devices that
 videodev supports, which is equal to VIDEO_NUM_DEVICES.
 */
static int __init ionvideo2_init(void)
{
    int ret = 0, i;
    ret = class_register(&ionvideo2_class);
    if(ret<0)
        return ret;

    ret = ionvideo2_create_instance();

    if (ret < 0) {
        printk(KERN_ERR "ionvideo2: error %d while loading driver\n", ret);
        return ret;
    }

    printk(KERN_INFO "Video Technology Magazine Ion Video "
            "Capture Board ver %s successfully loaded.\n",
            IONVIDEO_VERSION);

    return ret;
}

static void __exit ionvideo2_exit(void)
{
    ionvideo2_release();
	class_unregister(&ionvideo2_class);
}

MODULE_DESCRIPTION("Video Technology Magazine Ion Video Capture Board");
MODULE_AUTHOR("Amlogic, Shuai Cao<shuai.cao@amlogic.com>");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION(IONVIDEO_VERSION);

module_init (ionvideo2_init);
module_exit (ionvideo2_exit);
