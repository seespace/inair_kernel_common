#ifndef _IONVIDEO2_H
#define _IONVIDEO2_H

#include <linux/module.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/videodev2.h>
#include <linux/kthread.h>
#include <linux/freezer.h>
#include <linux/delay.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-fh.h>
#include <media/v4l2-event.h>
#include <media/v4l2-common.h>
#include <media/videobuf2-core.h>

#include <linux/mm.h>
#include <mach/mod_gate.h>

#include <linux/amlogic/amports/vframe.h>
#include <linux/amlogic/amports/vframe_provider.h>
#include <linux/amlogic/amports/vframe_receiver.h>
#include <linux/amlogic/ge2d/ge2d.h>
#include <linux/amlogic/amports/vframe.h>
#include <linux/amlogic/amports/canvas.h>

#include <linux/amlogic/amports/timestamp.h>
#include <linux/amlogic/amports/tsync.h>
#include "videobuf2-ion.h"

#define MAX_WIDTH 1920
#define MAX_HEIGHT 1088

#define PPMGR3_MAX_CANVAS 8
#define PPMGR3_CANVAS_INDEX AMLVIDEO2_RES_CANVAS // use amlvideo2 canvas

#define DUR2PTS(x) ((x) - ((x) >> 4))

#define dprintk(dev, level, fmt, arg...)                    \
    v4l2_dbg(level, debug, &dev->v4l2_dev, fmt, ## arg)

#define ppmgr3_printk(level, fmt, arg...)                   \
    do {                                                    \
        if (get_ionvideo2_debug() >= level)                  \
            printk(KERN_DEBUG "ppmgr3-dev: " fmt, ## arg);  \
    } while (0)

/* ------------------------------------------------------------------
 Basic structures
 ------------------------------------------------------------------*/

struct ionvideo2_fmt {
    char *name;
    u32 fourcc; /* v4l2 format id */
    u8 depth;
    bool is_yuv;
};

/* buffer for one video frame */
struct ionvideo2_buffer {
    /* common v4l buffer stuff -- must be first */
    struct vb2_buffer vb;
    struct list_head list;
    const struct ionvideo2_fmt *fmt;
    u64 pts;
    u32 duration;
};

struct ionvideo2_dmaqueue {
    struct list_head active;

    /* thread for generating video stream*/
    struct task_struct *kthread;
    wait_queue_head_t wq;
    /* Counters to control fps rate */
    int frame;
    int ini_jiffies;
};

struct ppmgr3_device {
    int dst_width;
    int dst_height;
    int ge2d_fmt;
    int canvas_id[PPMGR3_MAX_CANVAS];
    void* phy_addr[PPMGR3_MAX_CANVAS];
    int phy_size;

    ge2d_context_t* context;
    config_para_ex_t ge2d_config;

    int angle;
    int mirror;
    int paint_mode;
    int interlaced_num;
    int bottom_first;
};

struct ionvideo2_dev {
    struct list_head ionvideo2_devlist;
    struct v4l2_device v4l2_dev;
    struct video_device vdev;
    int fd_num;

    spinlock_t slock;
    struct mutex mutex;

    struct ionvideo2_dmaqueue vidq;

    /* Several counters */
    unsigned ms;
    unsigned long jiffies;

    /* Input Number */
    int input;

    /* video capture */
    const struct ionvideo2_fmt *fmt;
    unsigned int width, height;
    unsigned int c_width, c_height;
    struct vb2_queue vb_vidq;
    unsigned int field_count;

    unsigned int pixelsize;

    struct ppmgr3_device ppmgr3_dev;
    struct vframe_receiver_s video_vf_receiver;
    u64 pts;
    u8 receiver_register;
    u8 is_video_started;
    u32 skip;
    int once_record;
};

#endif
